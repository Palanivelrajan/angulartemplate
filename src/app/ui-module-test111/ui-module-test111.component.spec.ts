/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/22/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/22/2020</lastchangeddate>
*/

import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { UiModuleTest111Component } from './ui-module-test111.component';

describe('UiModuleTest111Component', () => {
  let spectator: Spectator<UiModuleTest111Component>;
  const createComponent = createComponentFactory({
    component: UiModuleTest111Component,
  });
  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

});
