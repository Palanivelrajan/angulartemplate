/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/24/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/24/2020</lastchangeddate>
*/

import { Spectator, createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { UiModule24JanService } from './ui-module24-jan.service';

describe('UiModule24JanService', () => {
  let spectator: SpectatorService<UiModule24JanService>;
  const createservice = createServiceFactory({
    service: UiModule24JanService,
  });
  beforeEach(() => {
    spectator = createservice();
  });

  it('should create', () => {
    expect(spectator.service).toBeTruthy();
  });

});
