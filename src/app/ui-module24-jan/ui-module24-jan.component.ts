/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/24/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/24/2020</lastchangeddate>
<description>this is cli</description>
*/

import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-ui-module24-jan',
  templateUrl: './ui-module24-jan.component.html',
})
export class UiModule24JanComponent implements OnInit, OnDestroy {

  ngOnInit(): void {
  }
  ngOnDestroy(): void  {
  }

}
