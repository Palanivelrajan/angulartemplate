/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/24/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/24/2020</lastchangeddate>
<description>this is cli</description>
*/

/**
 * Angular imports
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UiModule24JanComponent } from './ui-module24-jan.component';
import { UiModule24JanService } from './service/ui-module24-jan.service';
import { UiModule24JanRoutingModule } from './ui-module24-jan.route';
/**
 * Third party library imports
 */

/**
 * In house library imports
 */


/**
 * Application imports
 */


@NgModule({
declarations: [
   /**
    * Application imports
    */
   UiModule24JanComponent,

   /**
    * In house library imports
    */


   /**
    * Third party library imports
    */
],
imports: [
   /**
    * Application imports
    */
   UiModule24JanRoutingModule,

   /**
    * In house library imports
    */

   /**
    * Third party library imports
    */

   /**
    * Angular imports
    */
   BrowserModule,
   BrowserAnimationsModule,
 ],
providers: [

   /**
    * Application imports
    */
   UiModule24JanService,

    /**
     * In house library imports
     */


   /**
    * Third party library imports
    */
],

})
export class UiModule24JanModule {
}
