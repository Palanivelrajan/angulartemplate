/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/24/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/24/2020</lastchangeddate>
*/

import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { UiModule24JanComponent } from './ui-module24-jan.component';

describe('UiModule24JanComponent', () => {
  let spectator: Spectator<UiModule24JanComponent>;
  const createComponent = createComponentFactory({
    component: UiModule24JanComponent,
  });
  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

});
