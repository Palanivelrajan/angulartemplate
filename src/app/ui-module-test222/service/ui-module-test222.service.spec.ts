/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/23/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/23/2020</lastchangeddate>
*/

import { Spectator, createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { UiModuleTest222Service } from './ui-module-test222.service';

describe('UiModuleTest222Service', () => {
  let spectator: SpectatorService<UiModuleTest222Service>;
  const createservice = createServiceFactory({
    service: UiModuleTest222Service,
  });
  beforeEach(() => {
    spectator = createservice();
  });

  it('should create', () => {
    expect(spectator.service).toBeTruthy();
  });

});
