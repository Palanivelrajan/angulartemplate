/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/23/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/23/2020</lastchangeddate>
<description>This is cli for mudule</description>
*/

/**
 * Angular imports
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UiModuleTest222Component } from './ui-module-test222.component';
import { UiModuleTest222Service } from './service/ui-module-test222.service';
import { UiModuleTest222RoutingModule } from './ui-module-test222.route';
/**
 * Third party library imports
 */

/**
 * In house library imports
 */


/**
 * Application imports
 */


@NgModule({
declarations: [
   /**
    * Application imports
    */
   UiModuleTest222Component,

   /**
    * In house library imports
    */


   /**
    * Third party library imports
    */
],
imports: [
   /**
    * Application imports
    */
   UiModuleTest222RoutingModule,

   /**
    * In house library imports
    */

   /**
    * Third party library imports
    */

   /**
    * Angular imports
    */
   BrowserModule,
   BrowserAnimationsModule,
 ],
providers: [

   /**
    * Application imports
    */
   UiModuleTest222Service,

    /**
     * In house library imports
     */


   /**
    * Third party library imports
    */
],

})
export class UiModuleTest222Module {
}
