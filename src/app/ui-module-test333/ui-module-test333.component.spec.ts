/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/23/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/23/2020</lastchangeddate>
*/

import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { UiModuleTest333Component } from './ui-module-test333.component';

describe('UiModuleTest333Component', () => {
  let spectator: Spectator<UiModuleTest333Component>;
  const createComponent = createComponentFactory({
    component: UiModuleTest333Component,
  });
  beforeEach(() => {
    spectator = createComponent();
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

});
