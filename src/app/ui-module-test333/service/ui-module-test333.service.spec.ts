/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/23/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/23/2020</lastchangeddate>
*/

import { Spectator, createServiceFactory, SpectatorService } from '@ngneat/spectator';
import { UiModuleTest333Service } from './ui-module-test333.service';

describe('UiModuleTest333Service', () => {
  let spectator: SpectatorService<UiModuleTest333Service>;
  const createservice = createServiceFactory({
    service: UiModuleTest333Service,
  });
  beforeEach(() => {
    spectator = createservice();
  });

  it('should create', () => {
    expect(spectator.service).toBeTruthy();
  });

});
