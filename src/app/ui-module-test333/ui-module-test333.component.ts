/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/23/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/23/2020</lastchangeddate>
<description>This is cli</description>
*/

import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-ui-module-test333',
  templateUrl: './ui-module-test333.component.html',
})
export class UiModuleTest333Component implements OnInit, OnDestroy {

  ngOnInit(): void {
  }
  ngOnDestroy(): void  {
  }

}
