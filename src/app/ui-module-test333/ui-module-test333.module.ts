/*  Copyright (c) 2020 GEODIS.
All rights reserved. www.geodis.com
Reproduction or transmission in whole or in part, in any form or by
any means, electronic, mechanical or otherwise, is prohibited without the
prior written consent of the copyright owner.

<application>Synapse</application>
<author>Palanivelrajan LAKSHMANAN</author>
<createddate>01/23/2020</createddate>
<lastchangedby>Palanivelrajan LAKSHMANAN</lastchangedby>
<lastchangeddate>01/23/2020</lastchangeddate>
<description>This is cli</description>
*/

/**
 * Angular imports
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UiModuleTest333Component } from './ui-module-test333.component';
import { UiModuleTest333Service } from './service/ui-module-test333.service';
import { UiModuleTest333RoutingModule } from './ui-module-test333.route';
/**
 * Third party library imports
 */

/**
 * In house library imports
 */


/**
 * Application imports
 */


@NgModule({
declarations: [
   /**
    * Application imports
    */
   UiModuleTest333Component,

   /**
    * In house library imports
    */


   /**
    * Third party library imports
    */
],
imports: [
   /**
    * Application imports
    */
   UiModuleTest333RoutingModule,

   /**
    * In house library imports
    */

   /**
    * Third party library imports
    */

   /**
    * Angular imports
    */
   BrowserModule,
   BrowserAnimationsModule,
 ],
providers: [

   /**
    * Application imports
    */
   UiModuleTest333Service,

    /**
     * In house library imports
     */


   /**
    * Third party library imports
    */
],

})
export class UiModuleTest333Module {
}
